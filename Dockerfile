# Load the base pytorch image
FROM pytorch/pytorch:2.2.0-cuda12.1-cudnn8-devel

# Local arguments and envs
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PIP_ROOT_USER_ACTION=ignore
ARG DEBIAN_FRONTEND=noninteractive

# Update and install packages
RUN apt-get -y update && apt-get -y upgrade 
RUN apt-get -y install \
    build-essential \
    wget \
    curl \
    git \
    make \
    gcc \
    graphviz \
    sudo \
    h5utils \
    vim

# RUN conda update conda
# RUN conda update conda-build
RUN conda install pip

# Update python pip
RUN python -m pip install --upgrade pip
RUN python -m pip --version

# Install dependencies
COPY requirements.txt .
# RUN python -m pip install --upgrade -r requirements.txt
# RUN pip install --no-cache-dir --upgrade pip && pip install --no-cache-dir -r requirements.txt
RUN python -m pip install --upgrade pip
RUN python -m pip install --upgrade -r requirements.txt

# RUN conda install pyg -c pyg
# RUN conda install pytorch-sparse -c pyg
# RUN conda install pytorch-scatter -c pyg
# RUN conda install pytorch-cluster -c pyg
# RUN conda install pytorch-spline-conv -c pyg

RUN pip install pyg_lib torch_scatter torch_sparse torch_cluster torch_spline_conv --force-reinstall --no-cache-dir -f https://data.pyg.org/whl/torch-2.2.0+cu121.html